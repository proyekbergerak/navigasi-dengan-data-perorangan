import 'package:flutter/material.dart';
import 'package:latihan_navigasi/menu/setttings.dart';
import 'package:latihan_navigasi/screen1.dart';

void main() {
  runApp(const MainApp());
  
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  _MainAppState createState() => _MainAppState();
  /// ↓↓ ADDED
  /// InheritedWidget style accessor to our State object. 
  static _MainAppState of(BuildContext context) => 
      context.findAncestorStateOfType<_MainAppState>()!;
}

class _MainAppState extends State<MainApp> {
  late ThemeMode _themeMode = ThemeMode.system;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(),
      darkTheme: ThemeData.dark(),
      themeMode: _themeMode,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          actions: [
            PopupMenuButton<String>(
              onSelected: (value) {
                print('Selected value: $value');
                switch (value) {
                  case 'settings':
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder:(context)=> Settings() ),
                    );
                    break;
                  default:
                  break;
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                const PopupMenuItem<String>(
                  value: 'settings',
                  child: Text('Settings'),
                ),
              ],
            ),
          ],
        ),
        body: Screen1(),
      ),
    );
  }

  void changeTheme(ThemeMode themeMode) {
    setState(() {
      _themeMode = themeMode;
    });
  }
}
