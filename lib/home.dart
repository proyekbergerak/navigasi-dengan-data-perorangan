import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var skills = [
    {
      'skill': "Dart",
      'percentage': 70,
    },
    {
      'skill': "HTML",
      'percentage': 85,
    },
    {
      'skill': "PHP",
      'percentage': 85,
    },
    {
      'skill': "CSS",
      'percentage': 75,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[500],
      endDrawer: Drawer(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 24.0,
            ),
            child: ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(
                    "Home",
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 10.0,
                );
              },
              itemCount: 1,
            ),
          ),
        ),
      ),
      body: Container(
        child: ListView(
          children: [
            Center(
            child: Container(
            padding: EdgeInsets.all(15),
            child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.asset(
                      "images/meself.jpg",
                      width: 300.0,
                    ),
                  ),
                ),
                SizedBox(
                  height: 50.0,
                ),
                Center(
                  child: Text(
                    "Putra Wira",
                    style: GoogleFonts.lato(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 50.0,
                      height: 1.3,
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Email: johndoe@example.com',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Center(
                  child: Text(
                    'Phone: 08xxxxxxxxx',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                      GestureDetector(
                        child: CircleAvatar(
                          backgroundImage: AssetImage("images/wa.png"),
                        ),
                        onTap: () {
                          
                        },
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                        child: CircleAvatar(
                          backgroundImage:AssetImage("images/tiktok.png"),
                        ),
                        onTap: () {
                          
                        },
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                        child: CircleAvatar(
                          backgroundImage: AssetImage("images/ig.jpg"),
                        ),
                        onTap: () {
                          
                        },
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                        child: CircleAvatar(
                          backgroundImage: AssetImage("images/linkedIn.png"),
                        ),
                        onTap: () {
                          
                        },
                      ),
                    ],
                    ),
                  ),
                ],
                ),
                SizedBox(
                  width: 50,
                  child: Text(
                    """

                  """
                  ),
                ), 
                Text(
                  "SKILLS",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                    fontSize: 28.0,
                    height: 1.3,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "This is all the skills listed below more will be added in due time.",
                  style: TextStyle(
                    color: Colors.white,
                    height: 1.5,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Column(
                  children: [
                    for (int i = 0; i < skills.length; i++)
                      Container(
                        margin: EdgeInsets.only(bottom: 15.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex:
                                  int.parse(skills[i]['percentage'].toString()),
                              child: Container(
                                padding: EdgeInsets.only(left: 10.0),
                                alignment: Alignment.centerLeft,
                                height: 38.0,
                                child: Text(skills[i]['skill'].toString()),
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              // remaining (blank part)
                              flex: 100 -
                                  int.parse(skills[i]['percentage'].toString()),
                              child: Divider(
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              "${skills[i]['percentage']} %",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              ),
                            )
                          ],
                        ),
                      ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),    
      ],
      ),
    ),
    );
  }
}
