import 'package:flutter/material.dart';
import 'package:latihan_navigasi/main.dart';

class Settings extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Dark Mode',
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () => MainApp.of(context).changeTheme(ThemeMode.light),
                    child: Text('Light')),
                ElevatedButton(
                    onPressed: () => MainApp.of(context).changeTheme(ThemeMode.dark),
                    child: Text('Dark')),
              ],
            ),
          ],
        ),
      ),
    );
  }
}