// ignore_for_file: must_be_immutable, override_on_non_overriding_member

import 'package:flutter/material.dart';
import 'package:latihan_navigasi/contact.dart';
import 'package:latihan_navigasi/home.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class Screen1 extends StatelessWidget {
  Screen1({super.key});

  final _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          /*ElevatedButton(
          child: Container(child: Image.asset('images/google.webp')),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Screen2(gambar: 'facebook.png')));
          },
        ),
        ElevatedButton(
          child: Container(child: Image.asset('images/facebook.png')),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Screen2(gambar: 'facebook.png')));
          },
        ),*/
          PageView(
        controller: _pageController,
        children: [HomePage(), ContactPage()],
      ),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.grey.shade400,
        buttonBackgroundColor: Colors.white,
        height: 65,
        items: <Widget>[
          Icon(
            Icons.home,
            size: 35,
            color: Colors.grey.shade900,
          ),
          Icon(
            Icons.person,
            size: 35,
            color: Colors.grey.shade900,
          ),
        ],
        onTap: (index) {
          _pageController.animateToPage(index,
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeOut);
        },
      ),
    );
  }
}