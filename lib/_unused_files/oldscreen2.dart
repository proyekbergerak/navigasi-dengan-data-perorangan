import 'package:flutter/material.dart';

class Screen2 extends StatelessWidget {
  final String nama;
  final String npm;
  final String prodi;

  Screen2({
    super.key, 
    required this.nama, 
    required this.npm,
    required this.prodi,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:[ 
            Padding(padding: EdgeInsets.only(top: 20)),
            Center(
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(8)
                ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nama : $nama',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500
                    ),  
                  ),
                  SizedBox(height: 10,),
                    
                  Text(
                    'NPM : $npm',
                    style: TextStyle(
                    fontSize: 16,
                    ),  
                  ),
                  SizedBox(height: 10,),
                  
                  Text(
                    'Prodi : $prodi',
                    style: TextStyle(
                    fontSize: 16,
                    ),  
                  ),
                  SizedBox(height: 20,),
                ],
              ),
              ),
            ),
          Padding(padding: EdgeInsets.only(top: 20)),
          Center(
            child: GestureDetector(
              child: Container( 
                padding: const EdgeInsets.all(15), 
                color: Colors.lightBlue[600],  
                child: Text('Kembali'),
              ),
              onTap: (){
                Navigator.pop(context);
              } 
            ),
          ),
          ],
        ),
      ),
    );
  }
}