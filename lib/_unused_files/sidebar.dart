import 'package:flutter/material.dart';
import 'package:latihan_navigasi/contact.dart';

class SidebarMenu extends StatelessWidget {
  const SidebarMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            // Judul sidebar
            DrawerHeader(
              child: GestureDetector(
                child: Text("Kontak"),
                onTap: () {
                  ContactPage();
                },
              ),
            ),
            // Isi sidebar
            ListTile(
              title: Text('Profil'),
              onTap: () {},
            ),
            ListTile(
              title: Text('Statistik'),
              onTap: () {},
            ),
            ListTile(
              title: Text('Pengaturan'),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}