// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:latihan_navigasi/diff_contact.dart';

class ContactPage extends StatelessWidget {
  ContactPage({super.key});

  var mahasiswa = [
    {
      'npm' : '021210026',
      'nama' : 'Aisah',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/w3.jpg'
    },
    {
      'npm' : '011210046',
      'nama' : 'Femas Kurniawan',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/anjim.jfif'
    },
    {
      'npm' : '011210001',
      'nama' : 'Fernando',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/pristel.jpg'
    },
    {
      'npm' : '011210012',
      'nama' : 'Frima Mandala Putra',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/waduh.jpg'
    },
    {
      'npm' : '021210011',
      'nama' : 'Ingrid Indriani',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/w1.jpg'
    },
    {
      'npm' : '011210040',
      'nama' : 'Intan Seliandika',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/w2.jpg'
    },
    {
      'npm' : '021210003',
      'nama' : 'Kautsar Hidayatullah',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/slamet_kopling.jpg'
    },
    {
      'npm' : '021210068',
      'nama' : 'Leony Savayona',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/yanfei.jpg'
    },
    {
      'npm' : '021210010',
      'nama' : 'Miranda Rahma Puspita Sari',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/sucrose.png'
    },
    {
      'npm' : '011210042',
      'nama' : 'Muhamad Gymnastiar',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/terkadang.png'
    },
    {
      'npm' : '011210035',
      'nama' : 'Muhammad Alif Al Fajra',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/furina.jpg'
    },
    {
      'npm' : '011210022',
      'nama' : 'Muhammad Fajar Ikhwan',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/hemat-uang.jpg'
    },
    {
      'npm' : '021210072',
      'nama' : 'Muhammad Fajri Al Majid',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/apabila.jpg'
    },
    {
      'npm' : '021210045',
      'nama' : 'Nurjulianti',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/march.jpg'
    },
    {
      'npm' : '011210005',
      'nama' : 'Okta Pitriani',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/ayaka.jpg'
    },
    {
      'npm' : '011210009',
      'nama' : 'Prayoga Kurniawan',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/engiegame.jpg'
    },
    {
      'npm' : '011210023',
      'nama' : 'Putra Wira Albarokah',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/meself.jpg'
    },
    {
      'npm' : '011210043',
      'nama' : 'Rafly Dief Setiawan',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/kerja-bagus.jpg'
    },
    {
      'npm' : '021210042',
      'nama' : 'Rahman Ivan Nasikin',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/ivan.jpeg'
    },
    {
      'npm' : '021210061',
      'nama' : 'Rani Wiranda',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/yanfei.jpg'
    },
    {
      'npm' : '011210024',
      'nama' : 'Rily Dwi Marsela',
      'prodi' : 'Informatika Program Sarjana',
      'img' : 'images/yor.jpg'
    },
    {
      'npm' : '021210073',
      'nama' : 'Rini A\'yuni',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/yae.jpg'
    },
    {
      'npm' : '021210084',
      'nama' : 'Rizqi Irawan',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/gusus.jpg'
    },
    {
      'npm' : '021210023',
      'nama' : 'Supar Wadi',
      'prodi' : 'Sistem Informasi Program Sarjana',
      'img' : 'images/trollge.gif'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ListView(
            children: [
              for(int i = 0; i<mahasiswa.length;i++)
                InkWell(
                  onTap: () {
                  Navigator.push(context, 
                    MaterialPageRoute(builder: (context) => 
                      DetailContactPage(
                        nama: mahasiswa[i]['nama'].toString(),
                        npm: mahasiswa[i]['npm'].toString(),
                        prodi: mahasiswa[i]['prodi'].toString(),
                        img: mahasiswa[i]['img'].toString()
                      ),
                    ),
                  ); 
                  },child: Container(
                    height: 80,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      // color: (mahasiswa[i]['prodi'] != null && mahasiswa[i]['prodi'].toString() == 'Informatika Program Sarjana') ? Colors.blue : Colors.yellow,
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey,
                          width: 2
                        )
                      )
                    ),
                      child: ListTile(
                        leading: CircleAvatar(
                          foregroundImage: AssetImage(mahasiswa[i]['img'].toString()),
                        ),
                        title: Text(
                          mahasiswa[i]['nama'].toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Text(
                          mahasiswa[i]['prodi'].toString(),
                          ),
                        ),
                  ),
                ),
            ],
          ),
    );
  }
}